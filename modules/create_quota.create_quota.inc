<?php

/**
 * @file 
 * Create quota rules implementation for the create quota module.
 */

/**
 * Implementation of hook_create_quota_info().
 */
function create_quota_create_quota_info() {
  $items['create_quota_accumulate_content'] = array(
    'label' => t('Accumulate added content'),
    'help' => t('Every created content is counted.'),
  );
  return $items;  
}

function create_quota_accumulate_content($node, $account, $rule, $user_quota) {
  if (empty($user_quota)) {
    return array(
      'op' => 'insert',
      'quota' => array(
        'uid'       => $account->uid,
        'quota_rid' => $rule->rid,
        'quota'     => 1,
        'is_active' => TRUE,
      ),
    );
  }
  // Update only if user's quota is tracked.
  elseif ($user_quota->is_active) {
    $user_quota->quota++;
    return array(
      'op' => 'update',
      'quota' => $user_quota,
    );    
  }  
}
