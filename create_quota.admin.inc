<?php

/**
 * @file
 * Admin page callbacks for the create quota module.
 */

/**
 * Lists all current rules, and provides a link to the edit page.
 */
function create_quota_page() {
  $rules = _create_quota_load_all_rules(TRUE);

  $header = array(t('Label'), t('Quota'), t('Global'), t('Ops'));
  $rows = array();
  foreach ($rules as $rule) {
    $row = array();
    $row[] = check_plain($rule->label);
    $row[] = check_plain($rule->quota);
    $row[] = !empty($rule->global) ? t('Yes') : '';
    $row[] = l(t('edit'), 'admin/content/create_quota/edit/'. $rule->rid);
    $rows[] = $row;
  }
  if (count($rows) == 0) {
    $rows[] = array(
      array(
        'data' => t('No create quota rules have been defined.'),
        'colspan' => 3,
      ),
    );
  }
  $rows[] = array(
    array(
      'data' => l(t('Add a new create quota rule'), 'admin/content/create_quota/add'),
      'colspan' => 2,
    ),
  );
  return theme('table', $header, $rows);
}


/**
 * Displays an add/ edit form for a quota rule record.
 *
 * @param $form_state
 * @param $rid
 * @return unknown
 */
function create_quota_form(&$form_state, $rid = NULL) {
  $rules = _create_quota_load_all_rules();
  $disabled_global = FALSE;
  $global_rule = create_quota_get_global_rule();
  if (!empty($rid) && !empty($rules[$rid])) {
    $rule = $rules[$rid];
    $form['rid'] = array(
      '#type' => 'hidden',
      '#value' => $rid,
    );
    // This rule is the global rule.
    if (!empty($global_rule) && $global_rule->rid != $rid) {
      // There can be only a single global quota rule.
      $disabled_global = TRUE;
    }
  }
  elseif (!empty($global_rule)) {
    $disabled_global = TRUE;
  }

  foreach (create_quota_gather_data('create_quota_info') as $key => $value) {
    $options[$key] = $value['label'];
  }
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#description' => t("Enter the create quota rule name."),
    '#default_value' => !empty($rule->label) ? $rule->label : '',
    '#required' => TRUE,
  );
  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Quota type'),
    '#options' => $options,
    '#default_value' => !empty($rule->type) ? $rule->type : '',
    '#required' => TRUE,
  );
  $form['quota'] = array(
    '#type' => 'textfield',
    '#title' => t('Quota'),
    '#description' => t("Enter the quota of allowed content a user can create."),
    '#default_value' => !empty($rule->quota) ? $rule->quota : '',
    '#required' => TRUE,
  );

  $form['global'] = array(
    '#type' => 'checkbox',
    '#title' => t('Global rule'),
    '#description' => $disabled_global ? t("Quota rule <a href=\"!global-quota-rule\">@quota-rule-label</a> is already defined as a global quota rule and there can be only a single global rule, thus this field is disabled.", array('!global-quota-rule' => url('admin/content/create_quota/edit/'. $global_rule->rid), '@quota-rule-label' => $global_rule->label)) : t("If enabled all users that don't have specific quota rule assigned to them will be assigned this rule."),
    '#default_value' => !empty($rule->global) ? $rule->global : FALSE,
    '#disabled' => $disabled_global,
  );

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  if (!empty($rid)) {
    $form['buttons']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
    );
  }
  return $form;
}

function create_quota_form_submit(&$form, &$form_state) {
  if ($form_state['values']['op'] == t('Delete')) {
    $form_state['redirect'] = 'admin/content/create_quota/delete/'. $form_state['values']['rid'];
  }
  else {
    $rule = (object)$form_state['values'];
    _create_quota_save_rule($rule);
    $form_state['redirect'] = 'admin/content/create_quota';
  }
}

/**
 * Menu callback; Ask for confirmation for quota rule delete.
 */
function create_quota_delete_quota_confirm(&$form_state, $rid = NULL) {
  $rule = _create_quota_load_rule($rid);
  $form['rid'] = array(
    '#type' => 'value',
    '#value' => $rid,
  );
  return confirm_form($form,
    t('Are you sure you want to delete %rule?', array('%rule' => $rule->label)),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/content/create_quota',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete quota submit handler.
 */
function create_quota_delete_quota_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    _create_quota_delete_rule($form_state['values']['rid']);
    drupal_set_message(t('Quota has been deleted.'));
    $form_state['redirect'] = 'admin/content/create_quota';
  }
}

function create_quota_user_update($form_state, $account = NULL) {
  drupal_set_title(t("@account's quota", array('@account' => $account->name)));
  if (!$quota_rules = _create_quota_load_all_rules()) {
    $form['rule'] = array(
      '#type' => 'item',
      '#description' => t("No create <a href=\"!admin-content-create-quota-add\">quota rules</a> have been defined.", array('!admin-content-create-quota-add' => url('admin/content/create_quota/add'))),
    );
  }
  else {
    $user_quota  = _create_quota_get_user_quota_record($account);
    $options = array();
    foreach ($quota_rules as $rule) {
      if ($rule->global) {
        // Save the global rule for later use.
        $global_rule = $rule;
      }
      $options[$rule->global ? t('global') : t('not global')][$rule->rid] = $rule->label;
    }

    if (!empty($user_quota)) {
      $default_rid = $user_quota->quota_rid;
      // We will want to know later if to insert or update the record.
      $form['user_quota'] = array(
        '#type' => 'value',
        '#value' => TRUE,
      );
    }
    elseif ($global_rule) {
      // Assign user to the global rule.
      $default_rid = $global_rule->rid;
    }

    // Keep the uid for the form submit.
    $form['uid'] = array(
      '#type' => 'value',
      '#value' => $account->uid,
    );

    $form['quota_rid'] = array(
      '#type' => 'select',
      '#title' => t('Quota rule'),
      '#description' => t('Select the quota rule @account should be assigned to.', array('@account' => $account->name)),
      '#options' => $options,
      '#default_value' => !empty($default_rid) ? $default_rid : '',
      '#required' => TRUE,
      '#disabled' => empty($options),
    );
    $form['quota'] = array(
      '#type' => 'textfield',
      '#title' => t('Quota'),
      '#description' => t('Enter the number of content that should be assigned @account. When the content reaches the maximum allowed a Rules event will be triggered. 0 value means no content has been created yet.', array('@account' => $account->name)),
      '#default_value' => !empty($user_quota) ? $user_quota->quota : '0',
      '#required' => TRUE,
    );
    $form['is_active'] = array(
      '#type' => 'checkbox',
      '#title' => t('Is active'),
      '#description' => t('Should create quota module continue tracking @account content creation.', array('@account' => $account->name)),
      '#default_value' => !empty($user_quota) ? $user_quota->is_active : TRUE,
    );

    $form['buttons']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
    );
    if (!empty($user_quota)) {
      $form['buttons']['delete'] = array(
        '#type' => 'submit',
        '#value' => t('Delete'),
      );
    }
  }
  return $form;
}

function create_quota_user_update_submit(&$form, &$form_state) {
  if ($form_state['values']['op'] == t('Delete')) {
    $form_state['redirect'] = 'user/'. $form_state['values']['uid'] .'/create_quota/delete';
  }
  elseif (!empty($form_state['values']['quota'])) {
    $op = !empty($form_state['values']['user_quota']) ? 'update' : 'insert';
    $quota = array(
      'uid' => $form_state['values']['uid'],
      'quota_rid' => $form_state['values']['quota_rid'],
      'quota' => $form_state['values']['quota'],
      'is_active' => $form_state['values']['is_active'],
    );
    create_quota_save_user_quota(array('op' => $op, 'quota' => $quota));
    drupal_set_message(t('Quota has been updated.', array('@account' => $account->name)));
    $form_state['redirect'] = 'user/'. $form_state['values']['uid'];
  }
}

/**
 * Menu callback; Ask for confirmation for user quota delete.
 */
function create_quota_user_delete_confirm(&$form_state, $account = NULL) {
  $rule = _create_quota_get_user_quota_record($rid);
  $form['account'] = array(
    '#type' => 'value',
    '#value' => $account,
  );
  return confirm_form($form,
    t('Are you sure you want to delete %account quota?', array('%account' => $account->name)),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/content/create_quota',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete user quota submit handler.
 */
function create_quota_user_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $account = $form_state['values']['account'];
    create_quota_delete_user_quota($account);
    drupal_set_message(t('Quota has been deleted.'));
    $form_state['redirect'] = 'user/'. $account->uid;
  }
}