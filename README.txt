﻿Description
------------
Create quota module restricts content creation of users based on a quota.
A possible use case for this module is a site that allows users to register for free and to create a certain amount of content until they are asked to pay for the service.

This module has <a href="http://drupal.org/project/rules">Rules module</a> integration which extends the functionality.

Module setup
------------
1. Install the module.
2. Goto admin/content/create_quota and set the quota. This is the number of allowed content a user may create until she is restricted.
3 (optional). Install the Rules module. Use the "Create quota restricted creating content" event to act upon a content restriction.
