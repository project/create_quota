<?php

/**
 * @file
 * Rules integration for the create quota module.
 */

/**
 * Implementation of hook_rules_event_info().
 */
function create_quota_rules_event_info() {
  $info['create_quota'] = array(
    'label' => t('User reached maximum quota'),
    'help' => t("A user has reached it's <a href=\"@admin-content-create_quota\">create quota rule</a>.", array('@admin-content-create_quota' => url('admin/content/create_quota'))),
    'arguments' => array(
      'user_quota' => array(
        'type' => 'user_quota',
        'label' => t('User quota'),
      ),
    ) + rules_events_global_user_argument(),
    'module' => 'Create quota',
  );
  return $info;
}

/**
 * Implementation of hook_rules_action_info().
 */
function create_quota_rules_action_info() {
  $info['create_quota_rules_user_delete'] = array(
    'label' => t('Delete user quota'),
    'help' => t("Delete the user quota."),
    'arguments' => array(
      'user_quota' => array(
        'type' => 'user_quota',
        'label' => t('The user quota that will be deleted'),
      ),
    ),
    'module' => 'Create quota',
  );
  $info['create_quota_rules_user_disable'] = array(
    'label' => t('Stop user quota tracking'),
    'help' => t('User will be allowed to create content regardless of her user quota and her quota will not be updates.'),
    'arguments' => array(
      'user' => array(
        'type' => 'user_quota',
        'label' => t('User whose quota tracking will be stopped'),
      ),
    ),
    'module' => 'Create quota',
  );
  $info['create_quota_rules_user_enable'] = array(
    'label' => t('Restart user quota tracking'),
    'help' => t("The user's access to create a content will be determined by her access permissions and her user quota as-well."),
    'arguments' => array(
      'user_quota' => array(
        'type' => 'user_quota',
        'label' => t('The user quota that will be tracked'),
      ),
    ),
    'module' => 'Create quota',
  );
  $info['create_quota_rules_user_set_quota'] = array(
    'label' => t("Set the user quota - quota"),
    'help' => t("This action can be used to manually set the user's quota. For example it can be set to the maximum allowed, thus denying the permission of creating content."),
    'arguments' => array(
      'user_quota' => array(
        'type' => 'user_quota',
        'label' => t('The user quota that will be set'),
      ),
    ),
    'module' => 'Create quota',
  );
  $info['create_quota_rules_user_set_quota_rule'] = array(
    'label' => t("Set the user quota - rule"),
    'help' => t("This action can be used to manually set the user's quota rule."),
    'arguments' => array(
      'user_quota' => array(
        'type' => 'user_quota',
        'label' => t('The user quota that will be set'),
      ),
    ),
    'module' => 'Create quota',
  );  
  $info['create_quota_rules_load_user_quota'] = array(
    'label' => t("Load a user quota"),
    'help' => t("Use this action to enable other 'Create quota' module actions that require the user quota arguments if it isn't loaded yet."),
    'arguments' => array(
      'user' => array(
        'type' => 'user',
        'label' => t('The user that its user quota will be loaded'),
      ),
    ),
      'new variables' => array(
        'user_quota' => array(
          'type' => 'user_quota',
          'label' => t('User quota'),
        ),
      ),    
    'module' => 'Create quota',
  );  

  return $info;
}

/**
 * Action: Reset user quota.
 */
function create_quota_rules_user_delete($user_quota, $settings) {
  $user_quota->quota_rid = 0;
  return array('user_quota' => $user_quota);
}

/**
 * Action: Stop user quota tracking.
 */
function create_quota_rules_user_enable($user_quota, $settings) {
  $user_quota->is_active = TRUE;
  return array('user_quota' => $user_quota);
}

/**
 * Action: Restart user quota tracking.
 */
function create_quota_rules_user_disable($user_quota, $settings) {
  $user_quota->is_active = FALSE;
  return array('user_quota' => $user_quota);
}

function create_quota_rules_user_set_quota_form($settings, &$form) {
  $form['settings']['quota_value'] = array(
    '#type' => 'textfield',
    '#title' => t('User quota value'),
    '#default_value' => !empty($settings['quota_value']) ? $settings['quota_value'] : '',
    '#description' => t('Set to the value of content create quota'),
    '#required' => TRUE,
  );
}

function create_quota_rules_user_set_quota_validate($settings, &$form) {
  //TODO: use plugin validation.
  if ($form['values']['settings']['type'] == '1' && !is_numeric($form['values']['settings']['manual_value'])) {
    form_set_error('settings][manual_value', t('Manual value field requires a number.'));
  }
}

/**
 * Action: Set the user quota - quota.
 */
function create_quota_rules_user_set_quota($user_quota, $settings) {
  $user_quota->quota = $settings['quota_value'];
  return array('user_quota' => $user_quota);
}

/**
 * Action: Set the user's quota rule.
 */
function create_quota_rules_user_set_quota_rule_form($settings, &$form) {
  $options = array();
  foreach (_create_quota_load_all_rules(TRUE) as $value) {
    $options[$value->rid] = $value->label;
  }
  $form['settings']['quota_rid'] = array(
    '#type' => 'select',
    '#title' => t('User quota rule'),
    '#options' => $options,
    '#default_value' => !empty($settings['quota_rid']) ? $settings['quota_rid'] : '',
    '#description' => !empty($options) ? t('Select the quota rule that should be assigned to the user.') : t("There is no <a href=\"@admin-content-create_quota\">create quota rule</a> defined.", array('@admin-content-create_quota' => url('admin/content/create_quota'))),
    '#required' => TRUE,
    '#disabled' => empty($options),
  );  
}

/**
 * Action: Set the user quota - rule.
 */
function create_quota_rules_user_set_quota_rule($user_quota, $settings) {
  $user_quota->quota_rid = $settings['quota_rid'];
  return array('user_quota' => $user_quota);
}


/**
 * Action: Load a user's user quota.
 */
function create_quota_rules_load_user_quota($user) {
  if ($user_quota = _create_quota_get_user_quota_record($user)) {
    return $user_quota;
  }
  else {
    // Load default values for a new user quota. It's up to the user to change
    // set quota rule, otherwise this user quota wouldn't be saved.
    $user_quota->uid = $user->uid;
    $user_quota->quota_rid = 0;
    $user_quota->quota = 0;
    $user_quota->is_active = 1;
  }
  return array('user_quota' => $user_quota);
}

/**
 * Helper function; Insert or update a user record.
 *
 * @param $user
 *   The user object.
 * @param $enable
 *   Boolean indicating if user's quota tracking should be enabled.
 */
function _create_quota_rules_user_record($user_quota) {
  $user = user_load(array('uid' => $user_quota->uid));
  if (!_create_quota_get_user_quota_record($user)) {
    // Insert record.
    if ($user_quota->quota_rid != 0) {
      create_quota_save_user_quota(array('op' => 'insert', 'quota' => $user_quota));
    }
  }
  else {
    if ($user_quota->quota_rid == 0) {
      // Delete record.
      create_quota_delete_user_quota($user);      
    }
    else {
      // Update record.
      create_quota_save_user_quota(array('op' => 'update', 'quota' => $user_quota));      
    }
  }
}

/**
 * Implementation of hook_rules_data_type_info().
 */
function create_quota_rules_data_type_info() {
  return array(
    'user_quota' => array(
      'label' => t('User quota'),
      'class' => 'rules_data_type_user_quota',
      'savable' => TRUE,
      'identifiable' => TRUE,
      'module' => 'User quota',
    ),
  );
}

/**
 * Define the user_quota data type.
 */
class rules_data_type_user_quota extends rules_data_type {

  function save() {
    $user_quota = &$this->get();
    _create_quota_rules_user_record($user_quota);
    return TRUE;
  }

  function load($account) {
    return _create_quota_get_user_quota_record($account);
  }

  function get_identifier() {
    $user_quota = $this->get();
    return $user_quota->rid;
  }
}
